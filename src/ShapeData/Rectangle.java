package ShapeData;

import java.awt.*;

/**
 * a rectangle class extend from shape data class to draw a rectangle
 */
public class Rectangle extends ShapeData {
    /**
     * change draw method to draw a rectangle
     * @param graphics2D
     */
    public void draw(Graphics2D graphics2D){
        /**
         * set border color
         */
        graphics2D.setColor(new Color(borderR,borderG,borderB));
        /**
         * set stroke size
         */
        graphics2D.setStroke(new BasicStroke(5,BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        /**
         * draw a rectangle
         */
        graphics2D.drawRect(Math.min(x1, x2), Math.min(y1, y2),

                Math.abs(x1 - x2), Math.abs(y1 - y2));
        /**
         * fill background of this rectangle if it has a background color
         */
        if(hasBackgroundColor) {
            /**
             * set background color
             */
            graphics2D.setColor(new Color(backgroundR, backgroundG, backgroundB));
            /**
             * fill this rectangle
             */
            graphics2D.fillRect(Math.min(x1, x2), Math.min(y1, y2),

                    Math.abs(x1 - x2), Math.abs(y1 - y2));
        }

    }

}
