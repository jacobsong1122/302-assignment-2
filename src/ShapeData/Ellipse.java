package ShapeData;

import java.awt.*;

/**
 * a ellipse class extend form shape data class
 */
public class Ellipse extends ShapeData {
    /**
     * change draw method to draw a ellipse
     * @param graphics2D
     */
    public void draw(Graphics2D graphics2D){
        /**
         * set border color
         */
        graphics2D.setColor(new Color(borderR,borderG,borderB));
        /**
         * set stroke size
         */
        graphics2D.setStroke(new BasicStroke(5,BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        /**
         * draw oval
         */
        graphics2D.drawOval(Math.min(x1, x2), Math.min(y1, y2),
                Math.abs(x1 - x2), Math.abs(y1 - y2));
        /**
         * fill background if it has background color
         */
        if(hasBackgroundColor) {
            /**
             * set background color
             */
            graphics2D.setColor(new Color(backgroundR, backgroundG, backgroundB));
            /**
             * fill oval
             */
            graphics2D.fillOval(Math.min(x1, x2), Math.min(y1, y2),

                    Math.abs(x1 - x2), Math.abs(y1 - y2));
        }
    }


}