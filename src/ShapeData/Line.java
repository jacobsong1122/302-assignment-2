package ShapeData;

import java.awt.*;

/**
 * line shape class extend from shape data
 */
public class Line extends ShapeData {
    /**
     * change draw method to draw a line
     * @param graphics2D
     */
    public void draw(Graphics2D graphics2D){
        /**
         * set border color
         */
        graphics2D.setColor(new Color(borderR,borderG,borderB));
        /**
         * set stoke width
         */
        graphics2D.setStroke(new BasicStroke(5,BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        /**
         * draw line
         */
        graphics2D.drawLine(x1,y1,x2,y2);
    }

}