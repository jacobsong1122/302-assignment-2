package ShapeData;

import java.awt.*;

public class Polygon extends ShapeData {
    /**
     * count polygon knot
     */
    private int polygonCount = 0;

    /**
     * record Polygon co-ordinates
     */
    private int polygonx[] = new int[10000], polygony[] = new int[10000];
    /**
     * judge it is end point or not
     */
    private Boolean endPoint = false;

    /**
     * change draw method to draw a polygon
     * @param graphics2D
     */
    public void draw(Graphics2D graphics2D){
        /**
         * set border color
         */
        graphics2D.setColor(new Color(borderR,borderG,borderB));
        /**
         * set stroke size
         */
        graphics2D.setStroke(new BasicStroke(5,BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
        /**
         * draw polygon line
         */
        for(int i = 0; i < polygonCount+1;i++){
            graphics2D.drawLine(polygonx[i],polygony[i],polygonx[i+1],polygony[i+1]);
        }
        /**
         * fill the polygon if it has a background color
         */
        if(endPoint&&hasBackgroundColor) {
            /**
             * set background color
             */
            graphics2D.setColor(new Color(backgroundR,backgroundG,backgroundB));
            /**
             * fill polygon
             */
            graphics2D.fillPolygon(polygonx, polygony, polygonCount+1);
        }
    }
    /**
     * get polygon x
     */
    public int[] getPolygonX(){
        return polygonx;
    }
    /**
     * get polygon y
     */
    public int[] getPolygonY(){
        return polygony;
    }
    /**
     * change all coordinates
     */
    public void changePolygonCoordinates(int[] x, int[] y){
        this.polygonx = x;
        this.polygony = y;
    }

    /**
     * get polygon count
     * @return polygon count
     */
    public int getPolygonCount(){
        return polygonCount;
    }
    /**
     * increment polygon count
     */
    public void incrementPolygonCount(){
        this.polygonCount+=1;
    }

    /**
     * change polygon count
     */
    public void changePolygonCount(int polygonCount){
        this.polygonCount = polygonCount;
    }
    /**
     * change one polygon coordinates
     */
    public void setSinglePolygonCoordinates(int polygonCount, int polygonX, int polygonY){
        this.polygonx[polygonCount] = polygonX;
        this.polygony[polygonCount] = polygonY;
    }

    /**
     * get end point
     */
    public boolean getEndPoint(){
        return endPoint;
    }

    /**
     * set end point
     */
    public void setEndPoint(boolean endPoint){
        this.endPoint = endPoint;
    }

}
