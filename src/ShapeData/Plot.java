package ShapeData;

import java.awt.*;

/**
 * a plot class extend from shape data class to draw a plot
 */
public class Plot extends ShapeData {
    /**
     * change draw method to draw a plot
     * @param graphics2D
     */
    public void draw(Graphics2D graphics2D){
        /**
         * set plot color
         */
        graphics2D.setColor(new Color(borderR,borderG,borderB));
        /**
         * set plot size
         */
        graphics2D.setFont(new Font("TimesRoman", Font.PLAIN, 30));
        /**
         * draw a plot
         */
        graphics2D.drawString(".", x1, y1);

    }

    /**
     * change plot coordinates
     */
    public void changePlotCoordinates(int x1, int y1){
        this.x1 = x1;

        this.y1 = y1;

    }

}