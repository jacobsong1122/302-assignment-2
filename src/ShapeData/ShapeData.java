package ShapeData;

import java.awt.*;
import java.io.Serializable;

public abstract class ShapeData implements Serializable {
    /**
     * record normal co-ordinates
     */
    protected int x1, y1, x2, y2;
    /**
     * record border and background color, R,G,B
     */
    protected int borderR=0,borderG=0,borderB=0;
    protected int backgroundR=0,backgroundG=0,backgroundB=0;

    /**
     * judge it has background color or not
     */
    protected Boolean hasBackgroundColor = false;
    /**
     * judge it is polygon or not
     */
    protected Boolean isPolygon = false;
    /**
     * flag
     */
    protected String flag;

    /**
     * drawing function
     * @param graphic
     */
    public abstract void draw(Graphics2D graphic);





    /**
     * change flag
     * @param  flag
     */
    public void changeFlag(String flag){
        this.flag = flag;
    }
    /**
     * change coordinates
     */
    public void changeCoordinates(int x1, int y1,
                                  int x2,int y2){
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }

    /**
     * change coordinates
     */
    public void changeCoordinates2(int x2,int y2){

        this.x2 = x2;

        this.y2 = y2;
    }

    /**
     * get coordinates
     */
    public int getX1(){
        return x1;
    }
    public int getX2(){
        return x2;
    }
    public int getY1(){
        return y1;
    }
    public int getY2(){
        return y2;
    }

    /**
     * get color data
     */
    public int getBorderR(){
        return borderR;
    }
    public int getBorderG(){
        return borderG;
    }
    public int getBorderB(){
        return borderB;
    }
    public int getBackgroundR(){
        return backgroundR;
    }
    public int getBackgroundG(){
        return backgroundG;
    }
    public int getBackgroundB(){
        return backgroundB;
    }

    /**
     * set color data
     */
    public void setBorderR(int borderR){
        this.borderR = borderR;
    }
    public void setBorderG(int borderG){
        this.borderG = borderG;
    }
    public void setBorderB(int borderB){
        this.borderB = borderB;
    }
    public void setBackgroundB(int backgroundB){
        this.backgroundB= backgroundB;
    }
    public void setBackgroundG(int backgroundG){
        this.backgroundG= backgroundG;
    }
    public void setBackgroundR(int backgroundR){
        this.backgroundR= backgroundR;
    }

    /**
     * get flag
     */
    public String getFlag(){
        return flag;
    }
    /**
     * get has background color
     */
    public Boolean getHasBackgroundColor(){
        return hasBackgroundColor;
    }

    /**
     * set has background color
     * @param hasBackgroundColor
     */
    public void setHasBackgroundColor(Boolean hasBackgroundColor){
        this.hasBackgroundColor = hasBackgroundColor;
    }

    /**
     * get is polygon
     */
    public Boolean getIsPolygon(){
        return isPolygon;

    }




}
