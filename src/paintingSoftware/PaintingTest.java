package paintingSoftware;
import static org.junit.jupiter.api.Assertions.*;

import ShapeData.*;
import org.junit.jupiter.api.*;
public class PaintingTest {


    /**
     * initialize all shapes
     */
    Rectangle rectangle;
    Ellipse ellipse;
    Line line;
    Plot plot;
    Polygon polygon;
    Canvas canvas;
    /**
     * initialize shape box to null each time
     */
    @BeforeEach
    public void setUpShapes(){
        rectangle = null;
        ellipse = null;
        line = null;
        plot = null;
        polygon = null;
        canvas = null;
    }

    /**
     * test initial plot data
     * @throws Exception
     */
    @Test
    public void testPlot() throws Exception{
        plot = new Plot();
        assertEquals(plot.getX1(), 0);
        assertEquals(plot.getX2(), 0);
        assertEquals(plot.getY1(), 0);
        assertEquals(plot.getY2(), 0);
        assertEquals(plot.getBackgroundB(), 0);
        assertEquals(plot.getBackgroundG(), 0);
        assertEquals(plot.getBackgroundR(), 0);
        assertEquals(plot.getBorderB(), 0);
        assertEquals(plot.getBorderR(), 0);
        assertEquals(plot.getBorderG(), 0);
        assertNull(plot.getFlag());
        assertFalse(plot.getHasBackgroundColor());
        assertFalse(plot.getIsPolygon());
    }

    /**
     * Test change plot change flag
     * @throws Exception
     */
    @Test
    public void testPlotChangeFlag() throws Exception{
        plot = new Plot();
        plot.changeFlag("PLOT");
        assertEquals(plot.getFlag(), "PLOT");

    }

    /**
     * Test change plot change coordinates
     * @throws Exception
     */
    @Test
    public void testPlotChangeCoordinates() throws Exception{
        plot = new Plot();
        plot.changePlotCoordinates(10,20);
        assertEquals(plot.getX1(), 10);
        assertEquals(plot.getX2(), 0);
        assertEquals(plot.getY1(), 20);
        assertEquals(plot.getY2(), 0);

    }

    /**
     * test initial ellipse data
     * @throws Exception
     */
    @Test
    public void testEllipse() throws Exception{
        ellipse = new Ellipse();
        assertEquals(ellipse.getX1(), 0);
        assertEquals(ellipse.getX2(), 0);
        assertEquals(ellipse.getY1(), 0);
        assertEquals(ellipse.getY2(), 0);
        assertEquals(ellipse.getBackgroundB(), 0);
        assertEquals(ellipse.getBackgroundG(), 0);
        assertEquals(ellipse.getBackgroundR(), 0);
        assertEquals(ellipse.getBorderB(), 0);
        assertEquals(ellipse.getBorderR(), 0);
        assertEquals(ellipse.getBorderG(), 0);
        assertNull(ellipse.getFlag());
        assertFalse(ellipse.getHasBackgroundColor());
        assertFalse(ellipse.getIsPolygon());
    }

    /**
     * Test change ellipse change flag
     * @throws Exception
     */
    @Test
    public void testEllipseChangeFlag() throws Exception{
        ellipse = new Ellipse();
        ellipse.changeFlag("ELLIPSE");
        assertEquals(ellipse.getFlag(), "ELLIPSE");

    }

    /**
     * Test change ellipse change coordinates
     * @throws Exception
     */
    @Test
    public void testEllipseChangeCoordinates() throws Exception{
        ellipse = new Ellipse();
        ellipse.changeCoordinates(10,20,30,40);
        assertEquals(ellipse.getX1(), 10);
        assertEquals(ellipse.getX2(), 30);
        assertEquals(ellipse.getY1(), 20);
        assertEquals(ellipse.getY2(), 40);

    }
    /**
     * Test set ellipse has background color
     * @throws Exception
     */
    @Test
    public void testEllipseSetHasBackgroundColor() throws Exception{
        ellipse = new Ellipse();
        ellipse.setHasBackgroundColor(false);
        assertFalse(ellipse.getHasBackgroundColor());


    }
    /**
     * Test set ellipse has background color
     * @throws Exception
     */
    @Test
    public void testEllipseSetHasBackgroundColor1() throws Exception{
        ellipse = new Ellipse();
        ellipse.setHasBackgroundColor(true);
        assertTrue(ellipse.getHasBackgroundColor());


    }

    /**
     * test initial rectangle data
     * @throws Exception
     */
    @Test
    public void testRectangle() throws Exception{
        rectangle = new Rectangle();
        assertEquals(rectangle.getX1(), 0);
        assertEquals(rectangle.getX2(), 0);
        assertEquals(rectangle.getY1(), 0);
        assertEquals(rectangle.getY2(), 0);
        assertEquals(rectangle.getBackgroundB(), 0);
        assertEquals(rectangle.getBackgroundG(), 0);
        assertEquals(rectangle.getBackgroundR(), 0);
        assertEquals(rectangle.getBorderB(), 0);
        assertEquals(rectangle.getBorderR(), 0);
        assertEquals(rectangle.getBorderG(), 0);
        assertNull(rectangle.getFlag());
        assertFalse(rectangle.getHasBackgroundColor());
        assertFalse(rectangle.getIsPolygon());
    }

    /**
     * Test change rectangle change flag
     * @throws Exception
     */
    @Test
    public void testRectangleChangeFlag() throws Exception{
        rectangle = new Rectangle();
        rectangle.changeFlag("RECTANGLE");
        assertEquals(rectangle.getFlag(), "RECTANGLE");

    }

    /**
     * Test change rectangle change coordinates
     * @throws Exception
     */
    @Test
    public void testRectangleChangeCoordinates() throws Exception{
        rectangle = new Rectangle();
        rectangle.changeCoordinates(10,20,30,40);
        assertEquals(rectangle.getX1(), 10);
        assertEquals(rectangle.getX2(), 30);
        assertEquals(rectangle.getY1(), 20);
        assertEquals(rectangle.getY2(), 40);

    }
    /**
     * Test set rectangle has background color
     * @throws Exception
     */
    @Test
    public void testRectangleSetHasBackgroundColor() throws Exception{
        rectangle = new Rectangle();
        rectangle.setHasBackgroundColor(false);
        assertFalse(rectangle.getHasBackgroundColor());


    }
    /**
     * Test set rectangle has background color
     * @throws Exception
     */
    @Test
    public void testRectangleSetHasBackgroundColor1() throws Exception{
        rectangle = new Rectangle();
        rectangle.setHasBackgroundColor(true);
        assertTrue(rectangle.getHasBackgroundColor());


    }

    /**
     * test initial line data
     * @throws Exception
     */
    @Test
    public void testLine() throws Exception{
        line = new Line();
        assertEquals(line.getX1(), 0);
        assertEquals(line.getX2(), 0);
        assertEquals(line.getY1(), 0);
        assertEquals(line.getY2(), 0);
        assertEquals(line.getBackgroundB(), 0);
        assertEquals(line.getBackgroundG(), 0);
        assertEquals(line.getBackgroundR(), 0);
        assertEquals(line.getBorderB(), 0);
        assertEquals(line.getBorderR(), 0);
        assertEquals(line.getBorderG(), 0);
        assertNull(line.getFlag());
        assertFalse(line.getHasBackgroundColor());
        assertFalse(line.getIsPolygon());
    }

    /**
     * Test change line change flag
     * @throws Exception
     */
    @Test
    public void testLineChangeFlag() throws Exception{
        line = new Line();
        line.changeFlag("LINE");
        assertEquals(line.getFlag(), "LINE");

    }

    /**
     * Test change line change coordinates
     * @throws Exception
     */
    @Test
    public void testLineChangeCoordinates() throws Exception{
        line = new Line();
        line.changeCoordinates(10,20,30,40);
        assertEquals(line.getX1(), 10);
        assertEquals(line.getX2(), 30);
        assertEquals(line.getY1(), 20);
        assertEquals(line.getY2(), 40);

    }

    /**
     * test initial polygon data
     * @throws Exception
     */
    @Test
    public void testPolygon() throws Exception{
        polygon = new Polygon();
        assertEquals(polygon.getPolygonCount(), 0);
        assertEquals(polygon.getPolygonX()[0], 0);
        assertEquals(polygon.getPolygonY()[0], 0);
        assertFalse(polygon.getEndPoint());
        assertFalse(polygon.getHasBackgroundColor());
        assertEquals(polygon.getBackgroundB(), 0);
        assertEquals(polygon.getBackgroundG(), 0);
        assertEquals(polygon.getBackgroundR(), 0);
        assertEquals(polygon.getBorderB(), 0);
        assertEquals(polygon.getBorderR(), 0);
        assertEquals(polygon.getBorderG(), 0);
        assertNull(polygon.getFlag());
        assertFalse(polygon.getIsPolygon());
    }

    /**
     * Test change polygon change flag
     * @throws Exception
     */
    @Test
    public void testPolygonChangeFlag() throws Exception{
        polygon = new Polygon();
        polygon.changeFlag("POLYGON");
        assertEquals(polygon.getFlag(), "POLYGON");

    }


    /**
     * Test set polygon has background color
     * @throws Exception
     */
    @Test
    public void testPolygonSetHasBackgroundColor() throws Exception{
        polygon = new Polygon();
        polygon.setHasBackgroundColor(false);
        assertFalse(polygon.getHasBackgroundColor());


    }
    /**
     * Test set polygon has background color
     * @throws Exception
     */
    @Test
    public void testPolygonSetHasBackgroundColor1() throws Exception{
        polygon = new Polygon();
        polygon.setHasBackgroundColor(true);
        assertTrue(polygon.getHasBackgroundColor());


    }

    /**
     * Test set polygon change coordinates
     * @throws Exception
     */
    @Test
    public void testPolygonChangeCoordinates() throws Exception{
        polygon = new Polygon();
        int[] x = new int[10000];
        int[] y = new int[10000];
        x[0] = 1;
        y[0] = 1;
        polygon.changePolygonCoordinates(x,y);
        assertEquals(polygon.getPolygonX()[0], 1);
        assertEquals(polygon.getPolygonY()[0], 1);


    }
    /**
     * Test set polygon change count
     * @throws Exception
     */
    @Test
    public void testPolygonChangeCount() throws Exception{
        polygon = new Polygon();
        polygon.changePolygonCount(5);
        assertEquals(polygon.getPolygonCount(), 5);


    }
    /**
     * Test set polygon increment count
     * @throws Exception
     */
    @Test
    public void testPolygonIncrementCount() throws Exception{
        polygon = new Polygon();
        polygon.changePolygonCount(5);
        polygon.incrementPolygonCount();
        polygon.incrementPolygonCount();
        polygon.incrementPolygonCount();
        assertEquals(polygon.getPolygonCount(), 8);


    }
    /**
     * Test set polygon increment count
     * @throws Exception
     */
    @Test
    public void testSetSinglePolygonCoordinates() throws Exception{
        polygon = new Polygon();
        polygon.setSinglePolygonCoordinates(0, 10, 20);
        polygon.setSinglePolygonCoordinates(1, 20, 30);
        assertEquals(polygon.getPolygonX()[0], 10);
        assertEquals(polygon.getPolygonX()[1], 20);
        assertEquals(polygon.getPolygonY()[0], 20);
        assertEquals(polygon.getPolygonY()[1], 30);


    }

    /**
     * test initial canvas data
     * @throws Exception
     */
    @Test
    public void testCanvas() throws Exception{
        canvas = new Canvas();
        assertFalse(canvas.getAlreadySetIndex());
        assertTrue(canvas.getInitial());
        assertEquals(canvas.getBackgroundB(),0);
        assertEquals(canvas.getBackgroundG(),0);
        assertEquals(canvas.getBackgroundR(),0);
        assertEquals(canvas.getBorderB(),0);
        assertEquals(canvas.getBorderG(),0);
        assertEquals(canvas.getBorderR(),0);
        assertFalse(canvas.getHasBackgroundColor());
        assertFalse(canvas.getEndPoint());
        assertEquals(canvas.getFlag(),"RECTANGLE");
        assertEquals(canvas.getShapeIndex(),0);
        assertNull(canvas.getShapeData()[0]);

    }

    /**
     * test set specific shape data
     * @throws Exception
     */
    @Test
    public void testCanvasSetSpecificShapeData() throws Exception {
        canvas = new Canvas();
        plot = new Plot();
        canvas.setSpecificShapeData(0, new Plot());
        assertEquals(canvas.getShapeData()[0].getClass(), plot.getClass());

    }

    /**
     * test set specific shape data to null
     * @throws Exception
     */
    @Test
    public void testCanvasSetSpecificShapeDataToNull() throws Exception {
        canvas = new Canvas();
        canvas.setSpecificShapeData(0, new Plot());
        canvas.setSpecificShapeDataToNull(0);
        assertNull(canvas.getShapeData()[0]);

    }

    /**
     * test set flag
     * @throws Exception
     */
    @Test
    public void testCanvasSetFlag() throws Exception {
        canvas = new Canvas();
        canvas.setFlag("PLOT");
        assertEquals(canvas.getFlag(), "PLOT");

    }

    /**
     * test get specific shape data
     * @throws Exception
     */
    @Test
    public void testCanvasGetSpecificShapeData() throws Exception {
        canvas = new Canvas();
        plot = new Plot();
        canvas.setSpecificShapeData(0, new Plot());
        assertEquals(plot.getClass(), canvas.getSpecificShapeData(0).getClass());

    }

    /**
     * test increment shape index
     * @throws Exception
     */
    @Test
    public void testCanvasIncrementShapeIndex() throws Exception {
        canvas = new Canvas();
        canvas.incrementShapeIndex();
        canvas.incrementShapeIndex();
        assertEquals(canvas.getShapeIndex(), 2);

    }

    /**
     * test decrease shape index
     * @throws Exception
     */
    @Test
    public void testCanvasDecreaseShapeIndex() throws Exception {
        canvas = new Canvas();
        canvas.incrementShapeIndex();
        canvas.incrementShapeIndex();
        canvas.decreaseShapeIndex();
        assertEquals(canvas.getShapeIndex(), 1);

    }

    /**
     * Test set color for ellipse
     * @throws Exception
     */
    @Test
    public void testEllipseSetColor() throws Exception{
        ellipse = new Ellipse();
        ellipse.setBackgroundB(10);
        ellipse.setBackgroundR(20);
        ellipse.setBackgroundG(30);
        ellipse.setBorderB(10);
        ellipse.setBorderG(20);
        ellipse.setBorderR(30);
        assertEquals(ellipse.getBackgroundB(), 10);
        assertEquals(ellipse.getBackgroundR(), 20);
        assertEquals(ellipse.getBackgroundG(), 30);
        assertEquals(ellipse.getBorderB(), 10);
        assertEquals(ellipse.getBorderG(), 20);
        assertEquals(ellipse.getBorderR(), 30);

    }

    /**
     * Test set color for plot
     * @throws Exception
     */
    @Test
    public void testPlotSetColor() throws Exception{
        plot = new Plot();
        plot.setBackgroundB(10);
        plot.setBackgroundR(20);
        plot.setBackgroundG(30);
        plot.setBorderB(10);
        plot.setBorderG(20);
        plot.setBorderR(30);
        assertEquals(plot.getBackgroundB(), 10);
        assertEquals(plot.getBackgroundR(), 20);
        assertEquals(plot.getBackgroundG(), 30);
        assertEquals(plot.getBorderB(), 10);
        assertEquals(plot.getBorderG(), 20);
        assertEquals(plot.getBorderR(), 30);

    }

    /**
     * Test set color for line
     * @throws Exception
     */
    @Test
    public void testLineSetColor() throws Exception{
        line = new Line();
        line.setBackgroundB(10);
        line.setBackgroundR(20);
        line.setBackgroundG(30);
        line.setBorderB(10);
        line.setBorderG(20);
        line.setBorderR(30);
        assertEquals(line.getBackgroundB(), 10);
        assertEquals(line.getBackgroundR(), 20);
        assertEquals(line.getBackgroundG(), 30);
        assertEquals(line.getBorderB(), 10);
        assertEquals(line.getBorderG(), 20);
        assertEquals(line.getBorderR(), 30);

    }

    /**
     * Test set color for Rectangle
     * @throws Exception
     */
    @Test
    public void testRectangleSetColor() throws Exception{
        rectangle = new Rectangle();
        rectangle.setBackgroundB(10);
        rectangle.setBackgroundR(20);
        rectangle.setBackgroundG(30);
        rectangle.setBorderB(10);
        rectangle.setBorderG(20);
        rectangle.setBorderR(30);
        assertEquals(rectangle.getBackgroundB(), 10);
        assertEquals(rectangle.getBackgroundR(), 20);
        assertEquals(rectangle.getBackgroundG(), 30);
        assertEquals(rectangle.getBorderB(), 10);
        assertEquals(rectangle.getBorderG(), 20);
        assertEquals(rectangle.getBorderR(), 30);

    }

    /**
     * Test set color for polygon
     * @throws Exception
     */
    @Test
    public void testPolygonSetColor() throws Exception{
        polygon = new Polygon();
        polygon.setBackgroundB(10);
        polygon.setBackgroundR(20);
        polygon.setBackgroundG(30);
        polygon.setBorderB(10);
        polygon.setBorderG(20);
        polygon.setBorderR(30);
        assertEquals(polygon.getBackgroundB(), 10);
        assertEquals(polygon.getBackgroundR(), 20);
        assertEquals(polygon.getBackgroundG(), 30);
        assertEquals(polygon.getBorderB(), 10);
        assertEquals(polygon.getBorderG(), 20);
        assertEquals(polygon.getBorderR(), 30);

    }

    /**
     * Test polygon set endpoint
     * @throws Exception
     */
    @Test
    public void testPolygonSetEndPoint() throws Exception{
        polygon = new Polygon();
        polygon.setEndPoint(true);
        assertTrue(polygon.getEndPoint());
    }

    /**
     * Test canvas set color
     * @throws Exception
     */
    @Test
    public void testCanvasSetColor() throws Exception{
        canvas = new Canvas();
        canvas.setBackgroundB(10);
        canvas.setBackgroundG(20);
        canvas.setBackgroundR(30);
        canvas.setBorderB(10);
        canvas.setBorderG(20);
        canvas.setBorderR(30);
        assertEquals(canvas.getBackgroundB(), 10);
        assertEquals(canvas.getBackgroundG(), 20);
        assertEquals(canvas.getBackgroundR(), 30);
        assertEquals(canvas.getBorderB(), 10);
        assertEquals(canvas.getBorderG(), 20);
        assertEquals(canvas.getBorderR(), 30);
    }

    /**
     * Test save rectangle
     * @throws Exception
     */
    @Test
    public void testCanvasSaveRectangle() throws Exception{
        canvas = new Canvas();
        rectangle = new Rectangle();
        canvas.setFlag("RECTANGLE");
        canvas.setBorderB(20);
        canvas.setBorderG(20);
        canvas.setBorderR(20);
        canvas.setBackgroundB(10);
        canvas.setBackgroundR(10);
        canvas.setBackgroundG(10);
        canvas.setHasBackgroundColor(true);
        canvas.saveShapes();
        assertEquals(canvas.getSpecificShapeData(0).getClass(), rectangle.getClass());
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundB(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundG(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundR(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBorderB(), 20);
        assertEquals(canvas.getSpecificShapeData(0).getBorderG(), 20);
        assertEquals(canvas.getSpecificShapeData(0).getBorderR(), 20);
        assertTrue(canvas.getSpecificShapeData(0).getHasBackgroundColor());

    }

    /**
     * Test save plot
     * @throws Exception
     */
    @Test
    public void testCanvasSavePlot() throws Exception{
        canvas = new Canvas();
        plot = new Plot();
        canvas.setFlag("PLOT");
        canvas.setBorderB(20);
        canvas.setBorderG(20);
        canvas.setBorderR(20);
        canvas.setBackgroundB(10);
        canvas.setBackgroundR(10);
        canvas.setBackgroundG(10);
        canvas.saveShapes();
        assertEquals(canvas.getSpecificShapeData(0).getClass(), plot.getClass());
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundB(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundG(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundR(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBorderB(), 20);
        assertEquals(canvas.getSpecificShapeData(0).getBorderG(), 20);
        assertEquals(canvas.getSpecificShapeData(0).getBorderR(), 20);
        assertFalse(canvas.getSpecificShapeData(0).getHasBackgroundColor());

    }

    /**
     * Test save line
     * @throws Exception
     */
    @Test
    public void testCanvasSaveLine() throws Exception{
        canvas = new Canvas();
        line = new Line();
        canvas.setFlag("LINE");
        canvas.setBorderB(20);
        canvas.setBorderG(20);
        canvas.setBorderR(20);
        canvas.setBackgroundB(10);
        canvas.setBackgroundR(10);
        canvas.setBackgroundG(10);
        canvas.saveShapes();
        assertEquals(canvas.getSpecificShapeData(0).getClass(), line.getClass());
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundB(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundG(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundR(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBorderB(), 20);
        assertEquals(canvas.getSpecificShapeData(0).getBorderG(), 20);
        assertEquals(canvas.getSpecificShapeData(0).getBorderR(), 20);
        assertFalse(canvas.getSpecificShapeData(0).getHasBackgroundColor());

    }
    /**
     * Test save Ellipse
     * @throws Exception
     */
    @Test
    public void testCanvasSaveEllipse() throws Exception{
        canvas = new Canvas();
        ellipse = new Ellipse();
        canvas.setFlag("ELLIPSE");
        canvas.setBorderB(20);
        canvas.setBorderG(20);
        canvas.setBorderR(20);
        canvas.setBackgroundB(10);
        canvas.setBackgroundR(10);
        canvas.setBackgroundG(10);
        canvas.setHasBackgroundColor(true);
        canvas.saveShapes();
        assertEquals(canvas.getSpecificShapeData(0).getClass(), ellipse.getClass());
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundB(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundG(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundR(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBorderB(), 20);
        assertEquals(canvas.getSpecificShapeData(0).getBorderG(), 20);
        assertEquals(canvas.getSpecificShapeData(0).getBorderR(), 20);
        assertTrue(canvas.getSpecificShapeData(0).getHasBackgroundColor());

    }

    /**
     * Test set end point
     * @throws Exception
     */
    @Test
    public void testCanvasSetEndPoint() throws Exception{
        canvas = new Canvas();
        canvas.setEndPoint(true);
        assertTrue(canvas.getEndPoint());
    }

    /**
     * Test set initial
     * @throws Exception
     */
    @Test
    public void testCanvasSetInitial() throws Exception{
        canvas = new Canvas();
        canvas.setInitial(true);
        assertTrue(canvas.getInitial());
    }


    /**
     * Test save polygon
     * @throws Exception
     */
    @Test
    public void testCanvasSavePolygon() throws Exception{
        canvas = new Canvas();
        polygon = new Polygon();
        canvas.setFlag("POLYGON");
        canvas.setBorderB(20);
        canvas.setBorderG(20);
        canvas.setBorderR(20);
        canvas.setBackgroundB(10);
        canvas.setBackgroundR(10);
        canvas.setBackgroundG(10);
        canvas.setEndPoint(true);
        canvas.setHasBackgroundColor(true);
        canvas.saveShapes();
        assertEquals(canvas.getSpecificShapeData(0).getClass(), polygon.getClass());
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundB(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundG(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBackgroundR(), 10);
        assertEquals(canvas.getSpecificShapeData(0).getBorderB(), 20);
        assertEquals(canvas.getSpecificShapeData(0).getBorderG(), 20);
        assertEquals(canvas.getSpecificShapeData(0).getBorderR(), 20);
        assertTrue(canvas.getSpecificShapeData(0).getHasBackgroundColor());
        assertTrue(((Polygon)canvas.getSpecificShapeData(0)).getEndPoint());

    }
}