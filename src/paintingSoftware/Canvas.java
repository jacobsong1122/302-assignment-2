package paintingSoftware;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import ShapeData.*;
import ShapeData.Rectangle;
import ShapeData.Polygon;


/**
 *  Canvas class
 */
public class Canvas extends JPanel {
    /**
     * a boolean value to determine whether already set a
     * index value for Polygon or not
     */
    private Boolean alreadySetIndex = false;
    /**
     * judge this is initial shape index or not
     */
    private Boolean initial = true;
    /**
     * color r,g,b
     */
    private int borderR=0,borderG=0,borderB=0;
    private int backgroundR=0,backgroundG=0,backgroundB=0;
    /**
     * has background color and end point or not
     */
    private boolean hasBackgroundColor = false, endPoint = false;




    /**
     * create a text field to show the current status of the cursor
     */

    private TextField status = new TextField(30);
    /**
     * a flag to record each shape
     */
    private String flag = "RECTANGLE";
    /**
     * index of the shape
     */
    private int shapeIndex = 0;
    /**
     * shape data
     */
    private ShapeData[] shapeData = new ShapeData[10000];

    /**
     * Canvas constructor
     */
    public Canvas(){

        /**
         * set cursor style as cross hair
         */
        setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        /**
         * set Canvas background color to white
         */
        setBackground(Color.white);

        /**
         * add mouse listener
         */
        addMouseListener(new mouseAction());
        /**
         * add mouse listener
         */
        addMouseMotionListener(new mouseAction());



    }

    /**
     * override the paintComponent method
     */
    @Override
    public void paintComponent(Graphics graphics){
        super.paintComponent(graphics);
        Graphics2D graphics2D = (Graphics2D) graphics;
        int i = 0;
        /**
         * draw graphics
         */
        while(i<=shapeIndex){

            draw(graphics2D, shapeData[i]);
            i++;
        }


    }
    /**
     * mouse action class
     */
    public class mouseAction implements MouseListener, MouseMotionListener {
        public void mousePressed(MouseEvent e){
            /**
             * set end point to false
             */
            endPoint = false;

            if(flag == "POLYGON"){

                if(SwingUtilities.isLeftMouseButton(e)){

                    /**
                     * increment index if it has not been set
                     */
                    if(!alreadySetIndex){

                        if(!initial) {
                            shapeIndex++;
                        }

                        initial = false;


                        /**
                         * save shape data
                         */
                        saveShapes();
                        /**
                         * set cursor co-ordinates
                         */
                        ((Polygon)shapeData[shapeIndex]).
                                setSinglePolygonCoordinates(((Polygon)shapeData[shapeIndex]).getPolygonCount(),
                                        e.getX(),e.getY());
                        ((Polygon)shapeData[shapeIndex]).
                                setSinglePolygonCoordinates(((Polygon)
                                                shapeData[shapeIndex]).getPolygonCount()+1,
                                        e.getX(),e.getY());
                        alreadySetIndex = true;}
                    else{
                        ((Polygon)shapeData[shapeIndex]).
                                setSinglePolygonCoordinates(
                                        ((Polygon)shapeData[shapeIndex])
                                                .getPolygonCount()+1, e.getX(), e.getY());
                    }
                }

                /**
                 * save end points
                 */
                else if(SwingUtilities.isRightMouseButton(e)){

                    ((Polygon)shapeData[shapeIndex]).
                            setSinglePolygonCoordinates(
                                    ((Polygon)shapeData[shapeIndex])
                                            .getPolygonCount()+1, ((Polygon)shapeData[shapeIndex])
                                            .getPolygonX()[0], ((Polygon)shapeData[shapeIndex])
                                            .getPolygonY()[0]);
                    endPoint = true;
                    ((Polygon)shapeData[shapeIndex]).setEndPoint(true);

                }
            } else {
                /**
                 * increment one space to store the data of this shape
                 */
                if(!initial) {

                    shapeIndex++;
                }
                initial = false;
                /**
                 * save shape data
                 */
                saveShapes();

                /**
                 * set cursor co-ordinates
                 */
                shapeData[shapeIndex].changeCoordinates(e.getX(),e.getY(),e.getX(),e.getY());
            }


        }

        /**
         * set action when mouse release
         * @param e
         */
        public void mouseReleased(MouseEvent e){


            if (flag == "POLYGON") {
                if(SwingUtilities.isLeftMouseButton(e)) {
                    /**
                     * set the released cursor co-ordinates
                     */
                    ((Polygon)shapeData[shapeIndex]).
                            setSinglePolygonCoordinates(
                                    ((Polygon)shapeData[shapeIndex])
                                            .getPolygonCount()+1, e.getX(), e.getY());
                    ((Polygon)shapeData[shapeIndex]).incrementPolygonCount();
                }
                /**
                 * make set index to initial false status
                 */
                else if (SwingUtilities.isRightMouseButton(e)){

                    alreadySetIndex = false;

                }

            } else {
                /**
                 * set the released cursor co-ordinates
                 */
                shapeData[shapeIndex].changeCoordinates2(e.getX(),e.getY());
            }


        }



        @Override
        public void mouseEntered(MouseEvent e) {
            /**
             * set cursor status text
             */
            status.setText("The cursor has already entered the Canvas");
        }

        @Override
        public void mouseExited(MouseEvent e) {
            /**
             * set cursor status text
             */
            status.setText("The cursor has already existed the Canvas");
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            repaint();
        }

        /**
         * set drag action
         * @param e
         */
        @Override
        public void mouseDragged(MouseEvent e) {

            /**
             * set cursor status text
             */
            status.setText
                    ("The cursor dragged to position("+ e.getX() + "," + e.getY() + ")");

            /**
             * set drag coordinates
             */
            if(flag == "POLYGON") {
                if(SwingUtilities.isLeftMouseButton(e)) {
                    ((Polygon)shapeData[shapeIndex]).
                            setSinglePolygonCoordinates(
                                    ((Polygon)shapeData[shapeIndex])
                                            .getPolygonCount()+1, e.getX(), e.getY());
                }
                else if (SwingUtilities.isRightMouseButton(e)){

                }
            } else{
                shapeData[shapeIndex].changeCoordinates2(e.getX(),e.getY());
            }
            repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            /**
             * set cursor status text
             */
            status.setText
                    ("The cursor moved to position("+ e.getX() + "," + e.getY() + ")");
        }
    }


    /**
     * a function to save pictures on the Canvas
     */
    public void saveShapes(){

        switch (flag){
            case "RECTANGLE":
                shapeData[shapeIndex] = new Rectangle();
                shapeData[shapeIndex].changeFlag(flag);


                break;
            case "ELLIPSE":
                shapeData[shapeIndex] = new Ellipse();
                shapeData[shapeIndex].changeFlag(flag);

                break;
            case "LINE":
                shapeData[shapeIndex] = new Line();
                shapeData[shapeIndex].changeFlag(flag);

                break;
            case "PLOT":
                shapeData[shapeIndex] = new Plot();
                shapeData[shapeIndex].changeFlag(flag);

                break;
            case "POLYGON":
                shapeData[shapeIndex] = new Polygon();
                shapeData[shapeIndex].changeFlag(flag);
                ((Polygon)shapeData[shapeIndex]).setEndPoint(endPoint);
                break;
        }

        /**
         * change background and border color and set has
         * background color
         */
        shapeData[shapeIndex].setBorderR(borderR);
        shapeData[shapeIndex].setBorderG(borderG);
        shapeData[shapeIndex].setBorderB(borderB);

        shapeData[shapeIndex].setHasBackgroundColor(hasBackgroundColor);

        shapeData[shapeIndex].setBackgroundB(backgroundB);
        shapeData[shapeIndex].setBackgroundG(backgroundG);
        shapeData[shapeIndex].setBackgroundR(backgroundR);

    }

    /**
     * get shape data
     */
    public ShapeData[] getShapeData(){
        return shapeData;
    }
    /**
     * remove all shape data
     */
    public void removeAllShapeData(){
        /**
         * initialize all shape data again
         */
        shapeIndex = 0;
        shapeData[shapeIndex] = new Plot();
        borderB = 0;
        backgroundB = 0;
        borderR = 0;
        backgroundR = 0;
        borderG = 0;
        backgroundG = 0;
        hasBackgroundColor = false;

    }


    /**
     * a function to set flag
     */
    public void setFlag(String flag){
        this.flag = flag;
    }

    /**
     * draw shapes
     * @param graphics2D
     * @param shapeData
     */
    private void draw(Graphics2D graphics2D, ShapeData shapeData) {
        shapeData.draw(graphics2D);
    }


    /**
     * return status
     * @return
     */
    public TextField getStatus(){
        return this.status;
    }
    /**
     * get shape index
     */
    public int getShapeIndex(){
        return shapeIndex;
    }
    /**
     * set specific shape data
     */
    public void setSpecificShapeDataToNull(int index){
        this.shapeData[index] = null;
    }
    /**
     * set specific shape data
     */
    public void setSpecificShapeData(int index, ShapeData shapeData){
        this.shapeData[index] = shapeData;
    }
    /**
     * get specific shape data
     */
    public ShapeData getSpecificShapeData(int index){
        return shapeData[index];
    }


    /**
     * increment shape index
     */
    public void incrementShapeIndex(){
        shapeIndex++;
    }

    /**
     * decrease shape index
     */
    public void decreaseShapeIndex(){
        shapeIndex--;
    }

    /**
     * get already set index
     */
    public boolean getAlreadySetIndex(){
        return alreadySetIndex;
    }
    /**
     * get initial
     */
    public boolean getInitial(){
        return initial;
    }

    /**
     * get border color R
     */
    public int getBorderR(){
        return borderR;
    }
    /**
     * get border color G
     */
    public int getBorderG(){
        return borderG;
    }
    /**
     * get border color B
     */
    public int getBorderB(){
        return borderB;
    }
    /**
     * get background color R
     */
    public int getBackgroundR(){
        return backgroundR;
    }
    /**
     * get background color B
     */
    public int getBackgroundB(){
        return backgroundB;
    }
    /**
     * get background color G
     */
    public int getBackgroundG(){
        return backgroundG;
    }

    /**
     * set background color G
     */
    public void setBackgroundG(int backgroundG){
        this.backgroundG = backgroundG;
    }

    /**
     * set background color R
     */
    public void setBackgroundR(int backgroundR){
        this.backgroundR = backgroundR;
    }

    /**
     * set background color B
     */
    public void setBackgroundB(int backgroundB){
        this.backgroundB = backgroundB;
    }

    /**
     * set border color B
     */
    public void setBorderB(int borderB){
        this.borderB = borderB;
    }
    /**
     * set border color R
     */
    public void setBorderR(int borderR){
        this.borderR = borderR;
    }
    /**
     * set border color G
     */
    public void setBorderG(int borderG){
        this.borderG = borderG;
    }

    /**
     * get has background color
     */
    public boolean getHasBackgroundColor(){
        return hasBackgroundColor;
    }
    /**
     * get end point
     */
    public boolean getEndPoint(){
        return endPoint;
    }
    /**
     * get flag
     */
    public String getFlag(){
        return flag;
    }

    /**
     * set has background color
     */
    public void setHasBackgroundColor(boolean hasBackgroundColor){
        this.hasBackgroundColor = hasBackgroundColor;
    }

    /**
     * set end point
     */
    public void setEndPoint(boolean endPoint){
        this.endPoint = endPoint;
    }

    /**
     * set end point
     */
    public void setInitial(boolean initial){
        this.initial = initial;
    }

    /**
     * get end point
     */
    public void getEndPoint(boolean endPoint){
        this.endPoint = endPoint;
    }




}