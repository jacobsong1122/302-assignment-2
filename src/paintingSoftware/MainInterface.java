package paintingSoftware;
import ShapeData.Line;

import javax.swing.*;
import java.awt.*;

/**  create paintingSoftware.MainInterface extends JFrame and
 *  constructor, setLocation, close, size, visibility
 **/
public class MainInterface extends JFrame{
    /**
     * main interface constructor
     */
    public MainInterface() {
        super("Jacob Paint");
        //11
        /**
         * set close operation
         **/
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);




        /**
         * create a Canvas
         */
        Canvas canvas = new Canvas();

        canvas.saveShapes();
        /**
         * create a shape box
         **/
        CreateShapeBox shapeBox = new CreateShapeBox(canvas);
        /**
         * add shape box on the west side of the main panel
         **/
        add(shapeBox.getShapeBox(), "West");
        /**
         * create a color box panel
         */
        JPanel colorBox = new JPanel(new GridLayout(1, 2));
        /**
         * create a border shape box
         **/
        CreateColorBox borderColorBox = new CreateColorBox(canvas, "Border");
        /**
         * add shape box on the west side of the main panel
         **/
        colorBox.add(borderColorBox.getColorBox());
        /**
         * create a border shape box
         **/
        CreateColorBox backgroundColorBox = new CreateColorBox(canvas, "Background");
        /**
         * add shape box on the west side of the main panel
         **/
        colorBox.add(backgroundColorBox.getColorBox());
        /**
         * add color box on the main interface
         */
        add(colorBox, "East");
        /**
         * create a menu
         **/
        Menu menu = new Menu(canvas, this);
        /**
         * add menu on the north side of the main panel
         **/
        add(menu.getMenuPanel(), "North");





        /**
         * add Canvas on the center of the main panel
         */
        add(canvas, "Center");

        /**
         * add Canvas on the south of the main panel
         * and set its editable status to false,
         */
        add(canvas.getStatus(), "South");
        canvas.getStatus().setEditable(false);







        /**
         * set location
         **/

        setLocation(10, 10);

        /**
         * set size
         **/
        setPreferredSize(new Dimension(1000, 600));
        /**
         * pack all panels
         **/
        pack();
        /**
         * set visibility
         **/
        setVisible(true);

    }

    /**
     * a main function to run this main interface
     * @param args
     */
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable() {


            @Override
            public void run() {
                new MainInterface();
            }
        });
    }

}
