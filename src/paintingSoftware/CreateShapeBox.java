package paintingSoftware;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *   Create a shape Box and all panels and buttons
 */
public class CreateShapeBox extends JFrame {
    /**
     * Create a panel to support the whole shape box
     */
    private JPanel pnlShapeBox = new JPanel();
    /**
     * Create a panel to support Ellipse button
     */
    private JPanel pnlPlot = new JPanel();
    /**
     * Create a panel to support Line button
     */
    private JPanel pnlLine = new JPanel();
    /**
     * Create a panel to support Rectangle button
     */
    private JPanel pnlRectangle = new JPanel();
    /**
     * Create a panel to support Ellipse button
     */
    private JPanel pnlEllipse = new JPanel();
    /**
     * Create a panel to support Polygon button
     */
    private JPanel pnlPolygon = new JPanel();
    /**
     * Create a panel to support Polygon labe
     */
    private JPanel pnlPolygonLabel = new JPanel();
    /**
     * Create a Ellipse button
     */
    private JButton btnPlot = new JButton("Plot");
    /**
     * Create a Line button
     */
    private JButton btnLine = new JButton("Line");
    /**
     * Create a Rectangle button
     */
    private JButton btnRectangle = new JButton("Rectangle");
    /**
     * Create a Ellipse button
     */
    private JButton btnEllipse = new JButton("Ellipse");
    /**
     * Create a Polygon button
     */
    private JButton btnPolygon = new JButton("Polygon");
    /**
     * Create a Polygon comment
     */
    private JLabel lblPolygon = new JLabel("<html>Use left mouse to <br/>" +
            "draw polygon and <br/>right mouse to end <br/>your polygon</html>");

    /**
     *  Shape box constructor
     */
    public CreateShapeBox(Canvas canvas) {


        /**
         *  Set shape box layout.
         */
        pnlShapeBox.setLayout(new GridLayout(6, 1));
        /**
         *  Add buttons to their related panels
         */
        pnlPlot.add(btnPlot);
        pnlLine.add(btnLine);
        pnlRectangle.add(btnRectangle);
        pnlEllipse.add(btnEllipse);
        pnlPolygon.add(btnPolygon);
        pnlPolygonLabel.add(lblPolygon);


        /**
         *  Add all types of panels to the main shape box panel
         */
        pnlShapeBox.add(pnlPlot);
        pnlShapeBox.add(pnlLine);
        pnlShapeBox.add(pnlRectangle);
        pnlShapeBox.add(pnlEllipse);
        pnlShapeBox.add(pnlPolygon);
        pnlShapeBox.add(pnlPolygonLabel);

        /**
         *  Add action performing on each button
         */
        btnRectangle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setFlag("RECTANGLE");

            }
        });

        btnEllipse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setFlag("ELLIPSE");

            }
        });

        btnLine.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setFlag("LINE");

            }
        });

        btnPlot.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setFlag("PLOT");

            }
        });

        btnPolygon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.setFlag("POLYGON");

            }
        });

        /**
         *  Set a border for the shape box
         */
        pnlShapeBox.setBorder(BorderFactory.createTitledBorder("Shape Box"));


    }
    /**
     * Return shape box
     */
    public JPanel getShapeBox() {
        return pnlShapeBox;
    }
}