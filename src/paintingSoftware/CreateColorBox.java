package paintingSoftware;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *  (2) Create a shape Box and all panels and buttons
 */
public class CreateColorBox extends JFrame {
    /**
     * Create a panel to support the whole shape box
     */
    private JPanel pnlColorBox = new JPanel();
    /**
     * Create a panel to support Ellipse button
     */
    private JPanel pnlRed = new JPanel();
    /**
     * Create a panel to support Line button
     */
    private JPanel pnlCurrent = new JPanel();
    /**
     * Create a panel to support Rectangle button
     */
    private JPanel pnlBlue = new JPanel();
    /**
     * Create a panel to support Ellipse button
     */
    private JPanel pnlYellow = new JPanel();
    /**
     * Create a panel to support Polygon button
     */
    private JPanel pnlGreen = new JPanel();
    /**
     * Create a panel to support Polygon button
     */
    private JPanel pnlMore = new JPanel();
    /**
     * Create a panel to support Polygon button
     */
    private JPanel pnlEmpty = new JPanel();

    /**
     * Create a Ellipse button
     */
    private JButton btnYellow = new JButton();

    /**
     * Create a Line button
     */
    private JButton btnGreen = new JButton();
    /**
     * Create a Rectangle button
     */
    private JButton btnBlue = new JButton();
    /**
     * Create a Ellipse button
     */
    private JButton btnCurrent = new JButton();
    /**
     * Create a Polygon button
     */
    private JButton btnRed = new JButton();
    /**
     * Create a Polygon button
     */
    private JButton btnMore = new JButton("More");
    /**
     * Create a Polygon button
     */
    private JButton btnEmpty = new JButton("Empty");

    /**
     * (3) Shape box constructor
     */
    public CreateColorBox(Canvas canvas, String type) {


        /**
         *  Set shape box layout.
         */
        if(type == "Background") {
            pnlColorBox.setLayout(new GridLayout(7, 1));
        } else if(type == "Border"){
            pnlColorBox.setLayout(new GridLayout(6, 1));
        }
        /**
         * resize the buttons
         */

        btnCurrent.setPreferredSize(new Dimension(70,20));
        btnRed.setPreferredSize(new Dimension(70,20));
        btnBlue.setPreferredSize(new Dimension(70,20));
        btnGreen.setPreferredSize(new Dimension(70,20));
        btnYellow.setPreferredSize(new Dimension(70,20));
        btnMore.setPreferredSize(new Dimension(70,20));
        btnEmpty.setPreferredSize(new Dimension(70,20));


        /**
         *  Add buttons to their related panels
         */
        pnlGreen.add(btnGreen);
        pnlYellow.add(btnYellow);
        pnlBlue.add(btnBlue);
        pnlCurrent.add(btnCurrent);
        pnlRed.add(btnRed);
        pnlMore.add(btnMore);
        if(type == "Background") {
            pnlEmpty.add(btnEmpty);
        }
        /**
         * set background color
         */
        btnGreen.setBackground(Color.GREEN);
        btnYellow.setBackground(Color.YELLOW);
        btnBlue.setBackground(Color.BLUE);
        btnRed.setBackground(Color.RED);
        btnCurrent.setBackground(Color.BLACK);
        if(type == "Background") {
            btnCurrent.setBackground(Color.WHITE);
            btnCurrent.setText("N");
            btnEmpty.setBackground(Color.white);
        }


        /**
         *  Add all types of panels to the main shape box panel
         */
        pnlColorBox.add(pnlCurrent);

        pnlColorBox.add(pnlGreen);
        pnlColorBox.add(pnlYellow);
        pnlColorBox.add(pnlRed);

        pnlColorBox.add(pnlBlue);
        pnlColorBox.add(pnlMore);
        if(type == "Background") {
            pnlColorBox.add(pnlEmpty);
        }

        if(type == "Background") {
            btnEmpty.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    /**
                     * set has background color to false
                     */
                    canvas.setHasBackgroundColor(false);


                    /**
                     * set background color to white
                     */
                    btnCurrent.setBackground(Color.WHITE);
                    /**
                     * set text to N
                     */
                    btnCurrent.setText("N");

                }
            });
        }
        /**
         * set blue button action
         */
        btnBlue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /**
                 * change border color
                 */
                if(type == "Border") {
                    canvas.setBorderR(0);
                    canvas.setBorderG(0);
                    canvas.setBorderB(255);

                } else if(type == "Background"){
                    /**
                     * change background color
                     */
                    canvas.setHasBackgroundColor(true);
                    /**
                     * set text to empty
                     */
                    btnCurrent.setText("");
                    canvas.setBackgroundR(0);
                    canvas.setBackgroundG(0);
                    canvas.setBackgroundB(255);

                }
                /**
                 * set button background to blue
                 */
                btnCurrent.setBackground(Color.BLUE);

            }
        });

        /**
         * set red button action
         */
        btnRed.addActionListener(new ActionListener() {
            /**
             * change border and background color
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if(type == "Border") {
                    canvas.setBorderR(255);
                    canvas.setBorderG(0);
                    canvas.setBorderB(0);
                } else if(type == "Background"){
                    /**
                     * change background color
                     */
                    canvas.setHasBackgroundColor(true);
                    /**
                     * set text to empty
                     */
                    btnCurrent.setText("");
                    canvas.setBackgroundR(255);
                    canvas.setBackgroundG(0);
                    canvas.setBackgroundB(0);

                }
                /**
                 * set red button background color
                 */
                btnCurrent.setBackground(Color.RED);

            }
        });

        /**
         * set yellow button action
         */
        btnYellow.addActionListener(new ActionListener() {
            /**
             * set border and background color
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if(type == "Border") {
                    canvas.setBorderR(255);
                    canvas.setBorderG(255);
                    canvas.setBorderB(0);
                } else if(type == "Background"){
                    /**
                     * change background color
                     */
                    canvas.setHasBackgroundColor(true);
                    /**
                     * set text to empty
                     */
                    btnCurrent.setText("");
                    canvas.setBackgroundR(255);
                    canvas.setBackgroundG(255);
                    canvas.setBackgroundB(0);

                }
                /**
                 * change yellow button background
                 */
                btnCurrent.setBackground(Color.YELLOW);

            }
        });

        /**
         * set green button action
         */
        btnGreen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(type == "Border") {
                    canvas.setBorderR(0);
                    canvas.setBorderG(255);
                    canvas.setBorderB(0);
                } else if(type == "Background"){
                    /**
                     * change background color
                     */
                    canvas.setHasBackgroundColor(true);
                    /**
                     * set text to empty
                     */
                    btnCurrent.setText("");
                    canvas.setBackgroundR(0);
                    canvas.setBackgroundG(255);
                    canvas.setBackgroundB(0);

                }
                /**
                 * set green button background color
                 */
                btnCurrent.setBackground(Color.GREEN);


            }
        });
        /**
         * set more button action
         */
        btnMore.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                /**
                 * create a platte
                 */
                Color color=new Color(200,200,200);
                color=JColorChooser.showDialog(null,"Choose your favourite color" ,color);
                /**
                 * set border color
                 */
                if(type == "Border") {
                    canvas.setBorderR(color.getRed());
                    canvas.setBorderG(color.getGreen());
                    canvas.setBorderB(color.getBlue());
                    btnCurrent.setBackground(new Color(canvas.getBorderR()
                            ,canvas.getBorderG(),canvas.getBorderB()));
                } else if(type == "Background"){
                    /**
                     * change background color
                     */
                    canvas.setHasBackgroundColor(true);
                    /**
                     * set text to empty
                     */
                    btnCurrent.setText("");
                    canvas.setBackgroundR(color.getRed());
                    canvas.setBackgroundG(color.getGreen());
                    canvas.setBackgroundB(color.getBlue());
                    btnCurrent.setBackground(new Color(canvas.getBackgroundR(),
                            canvas.getBackgroundG(),canvas.getBackgroundB()));

                }




            }
        });


        /**
         *  Set a border for the shape box
         */
        if(type == "Border") {
            pnlColorBox.setBorder(BorderFactory.createTitledBorder("Border"));
        }else if(type == "Background"){
            pnlColorBox.setBorder(BorderFactory.createTitledBorder("Background"));
        }

    }

    /**
     * Return shape box
     */
    public JPanel getColorBox() {
        return pnlColorBox;
    }
}