package paintingSoftware;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Arrays;

import ShapeData.Polygon;
import ShapeData.Rectangle;
import ShapeData.Plot;

public class Menu extends Frame {
    JPanel menuPanel = new JPanel();


    Menu(Canvas canvas, MainInterface mainInterface){
        /**
         * create a new Menu bar
         */
        JMenuBar menuBar = new JMenuBar();

        JMenu menu = new JMenu("File");

        /**
         * create items
         */
        JMenuItem loadFileMenu = new JMenuItem("Load file");
        JMenuItem saveFileMenu = new JMenuItem("Save file");
        JMenuItem undoFileMenu = new JMenuItem("Undo");
        /**
         * add items on the menu
         */

        menuBar.add(loadFileMenu);
        menuBar.add(saveFileMenu);
        menuBar.add(undoFileMenu);
        /**
         * add keyboard control
         */
        undoFileMenu.setAccelerator(KeyStroke.getKeyStroke
                ('Z', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));


        /**
         * undo shapes
         */
        undoFileMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                canvas.setSpecificShapeDataToNull(canvas.getShapeIndex());
                if(canvas.getShapeIndex() != 0) {
                    canvas.decreaseShapeIndex();
                } else {
                    canvas.setInitial(true);
                    canvas.setSpecificShapeData(0, new Rectangle());
                }
                canvas.repaint();

            }
        });

        loadFileMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int canvasHeight = canvas.getHeight();
                int canvasWidth = canvas.getWidth();
                /**
                 * confirm with user if he want to save this file
                 */
                int loadFileValue = JOptionPane.showConfirmDialog(null,
                        "Save current file?", "Message", 0);
                /**
                 * save current file if user want to
                 */
                if(loadFileValue == 0){
                    saveFile(canvas);

                } else if(loadFileValue == 1){

                    /**
                     * remove all user drawn
                     */
                    canvas.removeAllShapeData();

                    canvas.repaint();
                    try{
                        /**
                         * choose the file user want to load
                         */
                        JFileChooser fileChooser = new JFileChooser();
                        fileChooser.setFileFilter(new FileFilter() {
                            @Override
                            public boolean accept(File f) {
                                if (f.isDirectory()) {
                                    return true;

                                }
                                /**
                                 * filter files
                                 */
                                else {
                                    String filename = f.getName().toLowerCase();
                                    return filename.endsWith(".vec") ;
                                }
                            }

                            @Override
                            public String getDescription() {
                                return null;
                            }
                        });
                        fileChooser.showOpenDialog(null);
                        File file = fileChooser.getSelectedFile();
                        if(file == null){
                            /**
                             * prompt user did not choose any file
                             */
                            JOptionPane.showMessageDialog(null,
                                    "You did not choose any file");
                        }
                        else{
                            /**
                             * create an input stream
                             */
                            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

                            /**
                             * create useful variables
                             */
                            String line;
                            String flag;

                            int count=0;
                            String x1="", x2="", y1="", y2="";
                            String x[]= new String[10000], y[] = new String[10000];
                            int[] polyX = new int[10000], polyY = new int[10000];

                            /**
                             * read each line
                             */
                            while ((line = bufferedReader.readLine()) != null) {
                                if(line.matches("(.*)POLYGON(.*)")
                                        || line.matches("(.*)LINE(.*)")
                                        || line.matches("(.*)PLOT(.*)")
                                        || line.matches("(.*)ELLIPSE(.*)")
                                        || line.matches("(.*)RECTANGLE(.*)")) {
                                    flag = "";
                                    x1 = "";
                                    x2 = "";
                                    y1 = "";
                                    y2 = "";
                                    x = new String[10000];
                                    y = new String[10000];
                                    polyX = new int[10000];
                                    polyY = new int[10000];
                                    /**
                                     * read flag
                                     */
                                    for (char word : line.toCharArray()) {
                                        if (word == ' ') {
                                            break;
                                        }
                                        flag += word;
                                    }
                                    /**
                                     * set flag
                                     */
                                    canvas.setFlag(flag);
                                    /**
                                     * save shapes
                                     */
                                    canvas.saveShapes();
                                    count = 0;
                                    /**
                                     * read polygon coordinates
                                     */
                                    if (flag.matches("(.*)POLYGON(.*)") ){
                                        for (char word : line.toCharArray()) {
                                            if (word == ' ') {
                                                if(count > 0){
                                                    if(count%2 == 1){
                                                        x[count/2] = x1;
                                                        x1 = "";
                                                    }else {
                                                        y[count / 2-1] = y1;
                                                        y1 = "";
                                                    }
                                                }
                                                count++;

                                            }
                                            if (word != ' ' && count > 0) {
                                                if (count%2 == 1) {
                                                    x1 += word;
                                                } else if(count%2 == 0){
                                                    y1 += word;
                                                }
                                            }
                                        }
                                        /**
                                         * set polygon coordinates
                                         */
                                        y[count / 2-1] = y1;
                                        x[count / 2] = x[0];
                                        y[count / 2] = y[0];
                                        for(int k = 0;k < count /2+1;k++){
                                            polyX[k] = (int) (Float.valueOf(x[k]) * canvasWidth);
                                            polyY[k] = (int) (Float.valueOf(y[k]) * canvasHeight);

                                        }
                                        /**
                                         * change polygon coordinates
                                         */
                                        ((Polygon)canvas.getSpecificShapeData(canvas.getShapeIndex()))
                                                .changePolygonCount(count/2-1);
                                        ((Polygon)canvas.getSpecificShapeData(canvas.getShapeIndex()))
                                                .changePolygonCoordinates(polyX, polyY);

                                    } else if(flag.matches("(.*)ELLIPSE(.*)")
                                            ||flag.matches("(.*)RECTANGLE(.*)")
                                            || flag.matches("(.*)LINE(.*)")){
                                        /**
                                         * read each line
                                         */
                                        for (char word : line.toCharArray()) {
                                            /**
                                             * record coordinates
                                             */
                                            if (word == ' ') {
                                                count++;
                                            }
                                            if (word != ' ') {
                                                switch (count) {
                                                    case 1:
                                                        x1 += word;
                                                        break;
                                                    case 2:
                                                        y1 += word;
                                                        break;
                                                    case 3:
                                                        x2 += word;
                                                        break;
                                                    case 4:
                                                        y2 += word;
                                                        break;
                                                }
                                            }
                                        }

                                        /**
                                         * change coordinates
                                         */
                                        canvas.getSpecificShapeData(canvas.getShapeIndex())
                                                .changeCoordinates((int)(Float.valueOf(x1)*canvasWidth),
                                                        (int)(Float.valueOf(y1)*canvasHeight),
                                                        (int)(Float.valueOf(x2)*canvasWidth),
                                                        (int)(Float.valueOf(y2)*canvasHeight));
                                    } else if(flag.matches("(.*)PLOT(.*)")){
                                        count = 0;
                                        /**
                                         * read each line
                                         */
                                        for (char word : line.toCharArray()) {
                                            if (word == ' ') {
                                                count++;
                                            }
                                            if (word != ' ') {
                                                switch (count) {
                                                    case 1:
                                                        x1 += word;
                                                        break;
                                                    case 2:
                                                        y1 += word;
                                                        break;
                                                }
                                            }
                                        }
                                        /**
                                         * change plot coordinates
                                         */
                                        ((Plot)canvas.getSpecificShapeData(canvas.getShapeIndex()))
                                                .changePlotCoordinates((int)(Float.valueOf(x1)*canvasWidth),
                                                        (int)(Float.valueOf(y1)*canvasHeight));
                                    }


                                    /**
                                     * increment shape index
                                     */
                                    canvas.incrementShapeIndex();

                                } else if(line.matches("(.*)PEN(.*)")){
                                    /**
                                     * read pen color
                                     */
                                    String penColor = "";
                                    int penCount = 0;
                                    for (char word : line.toCharArray()) {
                                        if(word == '#'){
                                            penCount++;
                                        }
                                        if(penCount==1){
                                            penColor += word;
                                        }
                                    }


                                    /**
                                     * transfer pen color to rgb
                                     */
                                    int  penR=  Integer.valueOf( penColor.substring( 1, 3 ), 16 );
                                    int  penG=  Integer.valueOf( penColor.substring( 3, 5 ), 16 );
                                    int  penB=  Integer.valueOf( penColor.substring( 5, 7 ), 16 );
                                    /**
                                     * set canvas border rgb
                                     */
                                    canvas.setBorderR(penR);
                                    canvas.setBorderG(penG);
                                    canvas.setBorderB(penB);

                                } else if(line.matches("(.*)FILL #(.*)")){
                                    /**
                                     * record fill color
                                     */
                                    String fillColor = "";
                                    int fillCount = 0;
                                    for (char word : line.toCharArray()) {
                                        if(word == '#'){
                                            fillCount++;
                                        }
                                        if(fillCount==1){
                                            fillColor += word;
                                        }
                                    }
                                    /**
                                     * transfer fill color
                                     */
                                    int  fillR=  Integer.valueOf( fillColor.substring( 1, 3 ), 16 );
                                    int  fillG=  Integer.valueOf( fillColor.substring( 3, 5 ), 16 );
                                    int  fillB=  Integer.valueOf( fillColor.substring( 5, 7 ), 16 );
                                    /**
                                     * change canvas background color
                                     * and set has background color and
                                     * end point to be true
                                     */
                                    canvas.setBackgroundR(fillR);
                                    canvas.setBackgroundG(fillG);
                                    canvas.setBackgroundB(fillB);
                                    canvas.setHasBackgroundColor(true);
                                    canvas.setEndPoint(true);

                                }else if(line.matches("(.*)FILL OFF(.*)")){
                                    /**
                                     * set has background color to false
                                     */

                                    canvas.setHasBackgroundColor(false);

                                }
                                /**
                                 * if this line is a continuous polygon coordinates
                                 */
                                else {

                                    /**
                                     * decrease shape index
                                     */
                                    canvas.decreaseShapeIndex();

                                    x1 = "";
                                    y1 = "";
                                    count++;

                                    /**
                                     * read each line
                                     */
                                    for (char word : line.toCharArray()) {
                                        if (word != ' ' && count > 0) {
                                            if (count%2 == 1) {
                                                x1 += word;
                                            } else if(count%2 == 0){
                                                y1 += word;
                                            }
                                        }
                                        if (word == ' ') {

                                            if(count%2 == 1){
                                                x[count/2] = x1;
                                                x1 = "";
                                            }else {
                                                y[count / 2-1] = y1;
                                                y1 = "";
                                            }

                                            count++;

                                        }

                                    }
                                    /**
                                     * set polygon coordinates
                                     */

                                    y[count / 2-1] = y1;
                                    x[count / 2] = x[0];
                                    y[count / 2] = y[0];
                                    for(int k = 0;k < count /2+1;k++){
                                        polyX[k] = (int) (Float.valueOf(x[k]) * canvasWidth);
                                        polyY[k] = (int) (Float.valueOf(y[k]) * canvasHeight);

                                    }
                                    /**
                                     * change polygon count
                                     */
                                    ((Polygon)canvas.getSpecificShapeData(canvas.getShapeIndex()))
                                            .changePolygonCount(count/2-1);
                                    ((Polygon)canvas.getSpecificShapeData(canvas.getShapeIndex()))
                                            .changePolygonCoordinates(polyX, polyY);
                                    canvas.incrementShapeIndex();
                                }


                            }
                        }
                        canvas.decreaseShapeIndex();

                        /**
                         * set flag to rectangle
                         */
                        canvas.setFlag("RECTANGLE");

                        if(canvas.getShapeIndex() == 0
                                && canvas.getSpecificShapeData(0).getFlag() == "RECTANGLE"
                                && canvas.getSpecificShapeData(0).getX1() == 0
                                && canvas.getSpecificShapeData(0).getX2() == 0
                                && canvas.getSpecificShapeData(0).getY1() == 0
                                && canvas.getSpecificShapeData(0).getY2() == 0){

                        } else {
                            /**
                             * set initial to be false
                             */
                            canvas.setInitial(false);
                        }
                        canvas.repaint();

                    }


                    catch (Exception e0){
                        e0.printStackTrace();
                    }
                }
            }
        });

        saveFileMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveFile(canvas);
            }
        });




        /**
         * add menu bar on the panel
         */
        menuPanel.add(menuBar);
    }


    /**
     * save files
     * @param canvas
     */
    public void saveFile(Canvas canvas) {
        int canvasHeight = canvas.getHeight();
        int canvasWidth = canvas.getWidth();

        /**
         * create a file chooser
         */
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                /**
                 * filter files
                 */
                else {
                    String filename = f.getName().toLowerCase();
                    return filename.endsWith(".vec") ;
                }
            }

            @Override
            public String getDescription() {
                return null;
            }
        });
        int userSelection = fileChooser.showOpenDialog(new JFrame());

        if(userSelection == JFileChooser.APPROVE_OPTION) {

            /**
             * create a file
             */
            File file = fileChooser.getSelectedFile();
            /**
             * set initial extension name
             */
            String filePath = file.getPath();
            if (!filePath.toLowerCase().endsWith(".vec")) {
                file = new File(filePath + ".vec");
            }
            /**
             *
             */
            try {
                FileWriter fileWriter = new FileWriter(file);
                /**
                 * create a buffer writer
                 */

                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                /**
                 * write data in this file
                 */
                for (int i = 0; i < canvas.getShapeIndex() + 1; i++) {
                    if (i > 0) {
                        /**
                         * write fill off
                         */
                        if ((canvas.getSpecificShapeData(i - 1).getHasBackgroundColor()
                                && !canvas.getSpecificShapeData(i).getHasBackgroundColor())
                                || (canvas.getSpecificShapeData(i - 1).getBackgroundR() !=
                                canvas.getSpecificShapeData(i).getBackgroundR()
                                || canvas.getSpecificShapeData(i - 1).getBackgroundG() !=
                                canvas.getSpecificShapeData(i).getBackgroundG()
                                || canvas.getSpecificShapeData(i - 1).getBackgroundB() !=
                                canvas.getSpecificShapeData(i).getBackgroundB())
                                && canvas.getSpecificShapeData(i - 1).getHasBackgroundColor()) {
                            bufferedWriter.write("FILL OFF");
                            bufferedWriter.newLine();
                        }
                    }
                    /**
                     * write pen
                     */
                    if (canvas.getSpecificShapeData(i).getBorderR() != 0
                            || canvas.getSpecificShapeData(i).getBorderG() != 0
                            || canvas.getSpecificShapeData(i).getBorderB() != 0) {
                        if (i == 0) {
                            bufferedWriter.write("PEN " + String.format("#%02x%02x%02x",
                                    canvas.getSpecificShapeData(i).getBorderR(),
                                    canvas.getSpecificShapeData(i).getBorderG(),
                                    canvas.getSpecificShapeData(i).getBorderB()).toUpperCase());
                            bufferedWriter.newLine();
                        } else {
                            /**
                             * write pen
                             */
                            if (canvas.getSpecificShapeData(i).getBorderR() !=
                                    canvas.getSpecificShapeData(i - 1).getBorderR()
                                    || canvas.getSpecificShapeData(i).getBorderG() !=
                                    canvas.getSpecificShapeData(i - 1).getBorderG()
                                    || canvas.getSpecificShapeData(i).getBorderB() !=
                                    canvas.getSpecificShapeData(i - 1).getBorderB()) {
                                bufferedWriter.write("PEN " + String.format("#%02x%02x%02x",
                                        canvas.getSpecificShapeData(i).getBorderR(),
                                        canvas.getSpecificShapeData(i).getBorderG(),
                                        canvas.getSpecificShapeData(i).getBorderB()).toUpperCase());
                                bufferedWriter.newLine();
                            }
                        }
                    }
                    if (canvas.getSpecificShapeData(i).getHasBackgroundColor()
                            && canvas.getSpecificShapeData(i).getFlag() != "LINE"
                            && canvas.getSpecificShapeData(i).getFlag() != "PLOT") {
                        if (i == 0) {
                            /**
                             * write fill for line and plot
                             */
                            bufferedWriter.write("FILL " + String.format("#%02x%02x%02x",
                                    canvas.getSpecificShapeData(i).getBackgroundR(),
                                    canvas.getSpecificShapeData(i).getBackgroundG(),
                                    canvas.getSpecificShapeData(i).getBackgroundB()).toUpperCase());
                            bufferedWriter.newLine();
                        } else {
                            /**
                             * write fill
                             */
                            if (canvas.getSpecificShapeData(i).getBackgroundR() !=
                                    canvas.getSpecificShapeData(i - 1).getBackgroundR()
                                    || canvas.getSpecificShapeData(i).getBackgroundG() !=
                                    canvas.getSpecificShapeData(i - 1).getBackgroundG()
                                    || canvas.getSpecificShapeData(i).getBackgroundB() !=
                                    canvas.getSpecificShapeData(i - 1).getBackgroundB()
                                    || canvas.getSpecificShapeData(i - 1).getHasBackgroundColor() == false) {
                                bufferedWriter.write("FILL " + String.format("#%02x%02x%02x",
                                        canvas.getSpecificShapeData(i).getBackgroundR(),
                                        canvas.getSpecificShapeData(i).getBackgroundG(),
                                        canvas.getSpecificShapeData(i).getBackgroundB()).toUpperCase());
                                bufferedWriter.newLine();
                            }
                        }
                    }
                    /**
                     * save polygon coordinates
                     */
                    if (canvas.getSpecificShapeData(i).getFlag().matches("(.*)POLYGON(.*)")) {
                        bufferedWriter.write(canvas.getSpecificShapeData(i).getFlag() + " ");
                        for (int j = 0; j < ((Polygon)canvas.getSpecificShapeData(i)).getPolygonCount() + 1; j++) {
                            bufferedWriter.write(
                                    String.format("%.2f", Float.valueOf((float)
                                            ((Polygon)canvas.getSpecificShapeData(i)).
                                                    getPolygonX()[j]/canvasWidth)) + " " +
                                            String.format("%.2f", Float.valueOf((float)
                                                    ((Polygon)canvas.getSpecificShapeData(i))
                                                            .getPolygonY()[j]/canvasHeight)));
                            if(j != ((Polygon)canvas.getSpecificShapeData(i)).getPolygonCount() + 1){
                                bufferedWriter.write(" ");
                            }
                        }

                    }
                    /**
                     * write plot coordinates
                     */
                    else if(canvas.getSpecificShapeData(i).getFlag() == "PLOT"){
                        bufferedWriter.write(canvas.getSpecificShapeData(i).getFlag() + " " +
                                String.format("%.2f", Float.valueOf((float)
                                        canvas.getSpecificShapeData(i).getX1()/canvasWidth)) + " " +
                                String.format("%.2f", Float.valueOf((float)
                                        canvas.getSpecificShapeData(i).getY1()/canvasHeight)) );
                    }
                    /**
                     * write else shapes' coordinates
                     */
                    else {
                        bufferedWriter.write(canvas.getSpecificShapeData(i).getFlag() + " " +
                                String.format("%.2f", Float.valueOf((float)
                                        canvas.getSpecificShapeData(i).getX1()/canvasWidth)) + " " +
                                String.format("%.2f", Float.valueOf((float)
                                        canvas.getSpecificShapeData(i).getY1()/canvasHeight))+ " " +
                                String.format("%.2f", Float.valueOf((float)
                                        canvas.getSpecificShapeData(i).getX2()/canvasWidth)) + " " +
                                String.format("%.2f", Float.valueOf((float)
                                        canvas.getSpecificShapeData(i).getY2()/canvasHeight)));
                    }

                    bufferedWriter.newLine();
                }

                if (canvas.getSpecificShapeData(canvas.getShapeIndex()).getHasBackgroundColor()) {
                    bufferedWriter.write("FILL OFF");
                }


                bufferedWriter.close();

            } catch (IOException e0) {
                e0.printStackTrace();
            }

        } else {
            /**
             * prompt user did not choose any file
             */
            JOptionPane.showMessageDialog(null,
                    "You cancelled");
        }




    }

    /**
     * return menu panel
     */
    public JPanel getMenuPanel(){
        return menuPanel;
    }
}
